# Migrating .NET

This is workshop given at DockerCon18 on June 12, 2018

http://dcus18.dwwx.space/

## 01 Intro
Welcome to the workshop :)

## 02 Pre-requisites
You will be provided with a virtual machine which is already prepped for the lab.

You will build images and push them to Docker Hub during the workshop, so they are available to use later. You'll need a Docker ID to push images.

Sign up for a free Docker ID on [Docker Hub](https://hub.docker.com/)

## 04 Workshop conventions
Exercises are shown like this:

This is something you do yourself...

```
copy and paste this code
```

## 04 Now - connect to your VM
You'll be given the connection details for your Windows Server 2016 VM during the workshop.

You can connect to the VM using:

* RDP (Windows)
* Microsoft Remote Desktop (Mac)
* Remmina (linux)

```
# RDP into the server VM. The server name will be something like:
# dwwx-dcus1800.centralus.cloudapp.azure.com
```

## 05 Clone the workshop source code

The VM has a bunch of tools and Docker images pre-installed. But the Git install failed, so you'll need to do that manually.

```PowerShell
choco install -y poshgit
$env:PATH="$env:PATH;C:\Program Files\Git\bin;"
cd C:\scm
git clone https://github.com/sixeyed/docker-windows-workshop.git
```

## 06 Install Docker Compose

```PowerShell
iwr -useb `
  https://raw.githubusercontent.com/sixeyed/docker-init/master/windows/install-docker-compose.ps1 `
  | iex
```


## 07 Update your VM setup
Now run a script to make sure everything is up to date.

The script will ask for your Docker ID - be sure to use your Docker ID (not your email address).

```PowerShell
cd C:\scm\docker-windows-workshop
.\lab-vm\update.ps1
```

Now close that PowerShell prompt.

Do not use PowerShell ISE for the workshop! It has a strange relationship with some docker commands, and you won't get far with it.


## 08 Log in to Docker Hub

Login to Docker Hub with your Docker credentials:

```PowerShell
docker login --username $env:dockerId
```

## 09 Check everything's OK

Now check the Docker setup by running a simple verify script.

```PowerShell
cd $env:workshop
   .\verify.ps1
```

You should see a Docker image gets built, a container is run and the image is pushed to Docker Hub.

# 10 We're ready!
Here we go :)

## 11 Building and Running ASP.NET WebForms Apps in Docker
Our demo app is a simple ASP.NET WebForms app which uses SQL Server for storage. It's a full .NET Framework app, which uses .NET version `4.7.2`.

Right now the web app is a monolith. By the end of the workshop we'll have broken it down, but first we need to get it running.

## 12 Build the web app image

Check out the [Dockerfile](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/frontend-web/web/Dockerfile) for the application. It uses Docker to compile the app from source, and package it into an image.

```PowerShell
docker image build -t dwwx/signup-web -f .\frontend-web\web\Dockerfile .
```

## 13 Run the web app

That's it!

You don't need Visual Studio or .NET 4.7.2 installed to build the app, you just need the source repo and Docker.

```PowerShell
# Try running the app in a container:
docker container run -d -p 8020:80 --name app dwwx/signup-web
```

# 14 Try it out

You can browse to port `8020` on your Docker host (that's your Windows Server 2016 VM or your Windows 10 laptop). Or you can browse direct to the container:

```PowerShell
# Get the container's IP address and launch the browser:
$ip = docker container inspect `
  --format '{{ .NetworkSettings.Networks.nat.IPAddress }}' app
firefox "http://$ip"
```

## 15 Tidy up before we try again

Oops.

Remember the app needs SQL Server, and there's no SQL Server on this machine. We'll run it properly next, but first let's clean up that container.

```PowerShell
# Remove the `app` container
docker container rm -f app
```

## 16: Run the app - with dependencies

Now we'll run the database in a container too - using Docker Compose to manage the whole app. Check out the [v1 manifest](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/app/v1.yml), it specifies SQL Server and the web app.

```PowerShell
# Now run the app using compose:
docker-compose -f .\app\v1.yml up -d
```

## 17 Try the app again
As before, browse to port `8020` on your Docker host or browse direct to the container:

```PowerShell
$ip = docker container inspect `
  --format '{{ .NetworkSettings.Networks.nat.IPAddress }}' app_signup-web_1
firefox "http://$ip"
```

## 18 Looking better :)
But let's check it really works. Click the Sign Up button, fill in the form and click Go! to save your details.

```PowerShell
docker container exec app_signup-db_1 powershell `
"Invoke-SqlCmd -Query 'SELECT * FROM Prospects' -Database SignUp"
```

## 19 All good
We're in a good place now. This could be a 10-year old WebForms app, and now you can run it in Docker and move it to the cloud - no code changes!

It's also a great starting point for modernizing the application.

## 20 Splitting Out the Application Homepage
Monoliths can run in containers just fine. But they aren't modern apps - they're just old apps running in containers.

You can rebuild a monolith into microservices, but that's a long-term project.

We'll do it incrementally instead, by breaking features out of the monolith and running them in separate containers - starting with the app's homepage

## 21 The new application homepage
Check out the [index.html](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/frontend-reverse-proxy/homepage/index.html) for the new homepage. It's a static HTML site which uses Vue.js.

The [Dockerfile](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/frontend-reverse-proxy/homepage/Dockerfile) is really simple - it just copies the HTML content into an IIS image.

It will run in its own container, so it can use a different technology stack from the main app.

```PowerShell
docker image build `
  -t dwwx/homepage `
  -f .\frontend-reverse-proxy\homepage\Dockerfile .
```

## 22 Run the new homepage
You can run the homepage on its own - great for fast iterating through changes.

```
docker container run -d -p 8040:80 --name home dwwx/homepage
```

## 23 Try it out
The homepage is available on port `8040` on your Docker host, so you can browse there or direct to the container:

```PowerShell
$ip = docker container inspect `
  --format '{{ .NetworkSettings.Networks.nat.IPAddress }}' home
firefox "http://$ip"
```

## 24 Almost there
The new homepage looks good, starts quickly and is packaged in a small Nano Server image.

It doesn't work on its own though.

To use the new homepage without changing the original app we can run a reverse proxy in another container.

## 25 The reverse proxy
We'll use [Nginx](http://nginx.org/en/) as the proxy. All requests will come to Nginx, and it will proxy content from the homepage container or the original app container, based on the requested route.

Nginx can do a lot more than that - in the [nginx.conf](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/frontend-reverse-proxy/reverse-proxy/conf/nginx.conf) configuration file we're setting up caching, and you can also use Nginx for SSL termination.

```PowerShell
docker image build `
  -t dwwx/reverse-proxy `
  -f .\frontend-reverse-proxy\reverse-proxy\Dockerfile .
```

## 26 Upgrade to use the new homepage
Now we can run the app and have content proxied by Nginx. Check out the [v2 manifest](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/app/v2.yml) - it adds services for the homepage and the proxy.

Only the reverse proxy has ports specified. It is the public entrypoint to the app, all other containers are internal - they can access each other, but the outside world can't get to them.

```
docker-compose -f .\app\v2.yml up -d
```

Compose compares the running state to the desired state in the manifest and starts new containers.

## 27 Check out the new integrated app
The reverse proxy is published to port `8020`, so you can browse there or to the new Nginx container:
```PowerShell
$ip = docker container inspect `
      --format '{{ .NetworkSettings.Networks.nat.IPAddress }}' app_proxy_1
    firefox "http://$ip"
```

Now you can click through to the original Sign Up page.

## 28 And just to be sure
Check nothing's broken.

Click the Sign Up! button, fill in the form and click Go! to save your details.

```PowerShell
docker container exec app_signup-db_1 powershell `
  "Invoke-SqlCmd -Query 'SELECT * FROM Prospects' -Database SignUp"
```

## 29 All good
So now we have a reverse proxy which lets us break UI features out of the monolith.

We're running a new homepage with Vue, but we could easily use a CMS for the homepage by running Umbraco in a container - and we could replace the Sign Up form with a separate component using Blazor.

These small units can be independently deployed, scaled and managed. That makes it easy to release front end changes without regression testing the whole monolith.

## 30 Breaking Out a Reference Data API
Docker makes it easy to run features in separate containers, and takes care of communication between containers.

Right now the web application loads reference data direct from the database - that's the list of countries and roles in the dropdown boxes.

We're going to provide that reference data through an API instead.

## 31 The reference data API
The new component is a simple REST API. You can browse the [source for the Reference Data API](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/signup/src/SignUp.Api.ReferenceData) - there's one controller to fetch countries, and another to fetch roles.

The API uses a new technology stack:

* [ASP.NET](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.1) Core as a fast, cross-platform alternative to full ASP.NET
* [Dapper](https://github.com/StackExchange/Dapper) as a fast, lightweight ORM

We can use new technologies without impacting the monolith, because this component runs in a separate container.

## 32 Build the API
Check out the [Dockerfile](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/backend-rest-api/reference-data-api/Dockerfile) for the API.

It uses the same principle to compile and package the app using containers, but the images use .NET Core running on Nano Server.

```PowerShell
docker image build `
  -t dwwx/reference-data-api `
  -f .\backend-rest-api\reference-data-api\Dockerfile .
```

## 33 Run the new API
You can run the API on its own, but it needs to connect to SQL Server.

The image bundles a default database connection string, and you can override it when you run containers with an environment variable.

```PowerShell
# Run the API, connecting it to the existing SQL container:
docker container run -d -p 8060:80 --name api `
  -e ConnectionStrings:SignUpDb="Server=signup-db;Database=SignUp;User Id=sa;Password=DockerCon!!!" `
  dwwx/reference-data-api
```

## 34 Try it out
The API is available on port `8060` on your Docker host, so you can browse there or direct to the container:


```PowerShell
$ip = docker container inspect `
  --format '{{ .NetworkSettings.Networks.nat.IPAddress }}' api
firefox "http://$ip/api/countries"
```
Replace `/countries` with `/roles` to see the other dataset

## 35 Upgrade to use the new API
Now we can run the app and have the reference data served by the API. Check out the [v3 manifest](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/app/v3.yml) - it adds a service for the REST API.

The manifest also configures the web app to use the API. This has to be a change to the monolith - in this case using Dependency Injection to load a different implementation of the reference data loader.

```PowerShell
# Upgrade to v3:
docker-compose -f .\app\v3.yml up -d
```

## 36 Try the new distributed app
The entrypoint is still the proxy listening on port `8020`, so you can browse there or to the container:

```PowerShell
$ip = docker container inspect `
      --format '{{ .NetworkSettings.Networks.nat.IPAddress }}' app_proxy_1
    firefox "http://$ip"
```

Now when you click through to the original Sign Up page, the dropdowns are loaded from the API.

## 37 Let's just check that
The new REST API writes log entries to the console, which Docker can read from the container.

The logs will show that the countries and roles controllers have been called - the request came from the web app.

```PowerShell
# Check the logs:
docker container logs app_reference-data-api_1
```

## 38 And just to be sure
The API uses a different ORM from the main app, but the entity classes are shared, so the reference data codes match up.

Click the *Sign Up!* button, fill in the form and click *Go!* to save your details.

```PowerShell
# Check the new data is there in the SQL container:
docker container exec app_signup-db_1 powershell `
  "Invoke-SqlCmd -Query 'SELECT * FROM Prospects' -Database SignUp"
```

## 39 All good
Now we've got a small, fast REST API providing a reference data service. It's only available to the web app right now, but we could easily make it publicly accessible.

How? Just by adding a new routing rule in the reverse proxy that's already part of our app. It could direct `/api` requests into the API container.

That's something you can try out yourself.

Hint: the `location` blocks in `nginx.conf` are where you need to start

## 40 Switching to Asynchronous Messaging
Right now the app saves data by synchronously connecting to SQL Server. That's a bottleneck which will stop the app performing if there's a peak in traffic.

We'll fix that by using a message queue instead - running in a Docker container.

When you sign up the web app will publish an event message on the queue, which a message handler picks up and actions. The message handler is a .NET Framework console app running in another container.

## 41 The save message handler
The new component is a simple .NET Console app. You can browse the [source for the save message handler](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/signup/src/SignUp.MessageHandlers.SaveProspect) - the work is all done in the `Program` class.

This is a full .NET Framework app, so it can continue to use the original Entity Framework logic from the monolith. It's a low-risk approach to updating the architecture.

## 42 Build the message handler
Check out the [Dockerfile](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/backend-async-messaging/save-handler/Dockerfile) for the message handler.

It uses the same principle to compile and package the app using containers, and the images use .NET Framework running on Windows Server Core.

```PowerShell
docker image build `
  -t dwwx/save-handler `
  -f .\backend-async-messaging\save-handler\Dockerfile .
```

## 43 Upgrade to use asynchronous messaging
Check out the [v4 manifest](https://github.com/sixeyed/docker-windows-workshop/blob/dcus18/app/v4.yml) - it adds services for the message handler and the message queue.

The message queue is [NATS](https://nats.io/), a high-performance in-memory queue which is ideal for communication between containers.

The manifest also configures the web app to use messaging. This has to be a change to the monolith - in this case using Dependency Injection to load a different implementation of the prospect save handler.

```PowerShell
# Upgrade to v4:
docker-compose -f .\app\v4.yml up -d
```

## 44 Check the message handler is listening
You now have a message queue and a message handler running in containers.

The message handler writes console log entries, so you can see that it has connected to the queue and is listening for messages.

```PowerShell
# Check the handler logs:
docker container logs app_signup-save-handler_1
```

You should see that the handler is connected and listening.


## 45 Try the new distributed app
The entrypoint is still the proxy listening on port `8020`, so you can browse there or to the container:

```PowerShell
$ip = docker container inspect `
    --format '{{ .NetworkSettings.Networks.nat.IPAddress }}' app_proxy_1
  firefox "http://$ip"
```

Now when you submit data, the web app publishes an event and the handler makes the database save

## 46 Try out the new version
Click the Sign Up! button, fill in the form and click Go! to save your details.

The UX is the same, but the save is asynchronous. You can see that in the logs for the message handler.

```PowerShell
# Check the handler logs:
docker container logs app_signup-save-handler_1
```

You should see that the handler has receievd and actioned a message, and it gets an ID back from the database

## 47 Let's just check that
To be sure, let's make sure the datahas really been saved in the database


```PowerShell
# Check the new data is there in the SQL container:
docker container exec app_signup-db_1 powershell `
  "Invoke-SqlCmd -Query 'SELECT * FROM Prospects' -Database SignUp"
```

## 48 All good
Now we've got an event driven architecture! Well, not completely - but for one key path through our application, we have event publishing.

You can easily extend the ap now by adding new message handlers which subscribe to the same event.

A new message handler could insert data into Elasticsearch and let users run their own analytics with Kibana. That's something you can try out yourself.

Hint: The code for that is in the workshop repo - `SignUp.MessageHandler.IndexProspect` is the place to start.

## 49 We're Done!
Thanks for coming to the workshop. We hope it was useful and we'll be glad to have your feedback.

The content for this workshop will stay online and you don't need a Windows Server VM to follow along - you can do everything with [Docker for Windows](https://www.docker.com/docker-windows) on Windows 10.

But before you go...

## 50 Next steps in your Docker journey

* Use Play with Docker
* Try one of the [Play with Docker labs](http://training.play-with-docker.com/)
* Follow [@EltonStoneman](https://twitter.com/EltonStoneman) and [@stefscherer](https://twitter.com/stefscherer) on Twitter
* Read [Docker on Windows](https://www.amazon.co.uk/Docker-Windows-Elton-Stoneman/dp/1785281658), the book
* Watch [Modernizing .NET Apps with Docker on Pluralsight](https://pluralsight.pxf.io/c/1197078/424552/7490?u=https%3A%2F%2Fwww.pluralsight.com%2Fcourses%2Fmodernizing-dotnet-framework-apps-docker), the video course (don't have Pluralsight? Ping @EltonStoneman on Twitter to get a free trial).
