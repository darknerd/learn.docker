# Notes around Windows

The .NET workshop was a failure: think 105 systems transferring gigabytes for Windows Remote Desktop sessions per minute.

## Links on Building WIndows Images

* Packer
    * Packer Templates: https://github.com/mwrock/packer-templates
    * Packer & Boxstarters: http://www.hurryupandwait.io/blog/creating-windows-base-images-for-virtualbox-and-hyper-v-using-packer-boxstarter-and-vagrant
    * Packer Build Instructions (Windows 2012r2): https://learn.chef.io/modules/local-development/windows/virtualbox/get-set-up#/
    * Getting Started with Chef on Windows Server
        * https://sammart.in/2014/08/24/getting-started-with-chef-on-windows-server/
        * https://sammart.in/2014/08/25/getting-started-with-chef-on-windows-server-part-2-chef-server-bootstrapping/
        * https://sammart.in/2014/10/19/getting-started-with-chef-on-windows-server-part-3-vagrant-windows-and-managed-chef/
        * https://sammart.in/getting-started-with-chef-on-windows-server-part-3a-packer-vagrant-and-vagrant-omnibus/
    * https://hodgkins.io/best-practices-with-packer-and-windows
* Chef
    * Nordstorm Bootstrapper: https://github.com/Nordstrom/chefdk_bootstrap#what-does-it-do
* Vagrant
    * WinRM: https://github.com/criteo/vagrant-winrm
    * Windows Host Issues:
        * https://github.com/hashicorp/vagrant/issues/6852
* Run Hyper-V + Virtualbox on System: http://rizwanansari.net/run-hyper-v-and-virtualbox-on-the-same-machine/
